/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.renu.oxgame;

/**
 *
 * @author ray
 */
public class Player {
    private char name;
    private int win;
    private int lose;
    private int drew;
    public Player(char name){
        this.name = name;
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public int getLose() {
        return lose;
    }

    public void setLose(int lose) {
        this.lose = lose;
    }

    public int getDrew() {
        return drew;
    }

    public void setDrew(int drew) {
        this.drew = drew;
    }
    
}
